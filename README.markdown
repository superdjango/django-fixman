# Django Fixture Manager

![](https://img.shields.io/badge/status-active-green.svg)
![](https://img.shields.io/badge/stage-development-blue.svg)
![](https://img.shields.io/badge/coverage-44%25-orange.svg)

A utility for managing Django fixture data.
